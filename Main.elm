module Main exposing (..)

import Slides exposing (md, mdFragments, slidesDefaultOptions)
import Slides.Styles
import Slides.SlideAnimation
import List
import Css exposing (..)
import Css.Elements exposing (img)


main =
    Slides.app
        { slidesDefaultOptions
            | style =
                List.append
                    [ img
                        [ maxWidth (px 500)
                        ]
                    ]
                <|
                    Slides.Styles.elmMinimalist (hex "#fff") (hex "#ccc") (px 16) (hex "#000")
            , slideAnimator = Slides.SlideAnimation.scroll
        }
        [ md
            """
        # Transactions and stuff...

        by Sean B. Powell <sean@tinyrhino.dk>


        """
        , md """
        # The generic example

        We're booking a vacation

        - Hotel
        - Flight
        - Car rental
        """
        , md
            """
            # The Problem

            Talking to webservices is a pain in the ass

            * External services fail
            * Latency is an issue
            * No ACID guarantee
            * Stuff is bound to go wrong
            """
        , md
            """
        # The Question

        How do other people maintain data consistency for operations that span multiple services?

        """
        , md
            """
        # Sagas

        Some people had some thoughts about long running transactions back in the dark ages

        They wrote a paper about it in (1987)

        We can use some of these ideas for distributed stuff..

        """
        , md """
        # The basic idea

        * Split long running business transactions into a series of sub-transactions (A saga)
        * Execute these transactions
        * On failure every sub-transaction fires off a compensation action, or retries from a savepoint
        """
        , md
            """

        # Sucess looks like this

        ```
        [Saga start] -> [T1] -> [T2] -> [T3] -> [T4] -> [Saga end]
        ```

        <br><br>Yay, we're going on vacation.

        """
        , md
            """

        # Failure looks like this

        ```
        [Saga start] -> [T1] -> [T2] -> [T3 fails]
                                            ↓
        [Saga end] <- [C1] <- [C2] <- [C3] <-
        ```
        <br><br>Oh no, somebody booked the last hotel room. Abort!

        """
        , md
            """

        # So how does it work?

        1. We log the sagas state to a persistant store
        2. There's an organizing process
        3. There are some transactions
        """
        , md """
        #  How does it look?

        ```
            def order_trip(uuid, data):
                saga = Saga.create(uuid, data)
                saga.run(
                    [
                        HandlePayment,
                        async(
                            BookHotel
                            BookCar,
                        ),
                        BookFlight,
                        Customer.send_succes_email
                    ], data
                )

                return saga.complete()
        ```
        """
        , md
            """
            # Maybe..
        """
        , md
            """
        Some links to click on

        http://www.cs.cornell.edu/andru/cs711/2002fa/reading/sagas.pdf

        https://www.youtube.com/watch?v=xDuwrtwYHu8

        https://docs.microsoft.com/en-us/azure/architecture/patterns/compensating-transaction
        """
        ]
